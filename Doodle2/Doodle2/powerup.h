//
//  powerup.h
//  Doodle2
//
//  Created by Neo SX on 2/26/17.
//  Copyright © 2017 Binghamton CSD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Powerup : UIView
@property (nonatomic) float dx, dy;  // Velocity

@end
