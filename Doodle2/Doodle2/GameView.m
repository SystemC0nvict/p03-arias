//
//  GameView.m
//  Doodle2
//
//  Created by Patrick Madden on 2/4/17.
//  Copyright © 2017 Binghamton CSD. All rights reserved.
//

#import "GameView.h"

//@interface GameView()
//@property (assign) SystemSoundID sound;
//@end
@implementation GameView
@synthesize jumper, bricks, pow;
@synthesize tilt;
@synthesize bheight;
@synthesize bcount, subimage, mess;

int bounce = 15;
int pcount = 0;

-(id)initWithCoder:(NSCoder *)aDecoder
{
    //height = 0;
    bounce = 15;
    //NSString *imagePath = [[NSBundle mainBundle] pathForResource:@"kappa" ofType:@"png"];
    //UIImage *imageObj = [[UIImage alloc]initWithContentsOfFile:imagePath];
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        CGRect bounds = [self bounds];
        
       // [self setBackgroundColor:[UIColor whiteColor]];
        [self setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"space.jpg"]]];
        //self.subimage = imageObj;
     
       jumper = [[Jumper alloc] initWithFrame:CGRectMake(bounds.size.width/2, bounds.size.height - 20, 26, 28)];
        //[jumper setBackgroundColor:[UIColor redColor]];
        [jumper setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"kappav2.png"]]];
        //[jumper setBackgroundColor:[UIColor colorWithPatternImage:imageObj]];
        //[jumper changePic];
        [jumper setDx:0];
        [jumper setDy:10];
        //[jumper changePic];
        [self addSubview:jumper];
        [self makeBricks:nil];

        //height = 0;
        bounce = 30;
    }
    return self;
}

-(IBAction)makeBricks:(id)sender
{
    CGRect bounds = [self bounds];
    float width = bounds.size.width * .2;
    float height = 20;
    [pow removeFromSuperview];
    if (bricks)
    {
        for (Brick *brick in bricks)
        {
            [brick removeFromSuperview];
        }
    }
        
    bricks = [[NSMutableArray alloc] init];
    float lasty = CGRectGetMaxY(self.frame) - 40;
    float lastx = rand() % (int)(bounds.size.width * .8);
    
    //for (int i = 0; i < 10; ++i)
   // int i = 0;
        while((lasty - 100) >= 0){
            Brick *b = [[Brick alloc] initWithFrame:CGRectMake(0, 0, width, height)];
           // [b setBackgroundColor:[UIColor orangeColor]];
            [b setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bricksv2.jpg"]]];
            [self addSubview:b];
            int ycoor = rand() % (100);
            ycoor =lasty - ycoor;
            int xcoor = rand() % (int)(bounds.size.width * .8) ;
            
            [b setCenter:CGPointMake(lastx, lasty)];
            /*[b setCenter:CGPointMake(rand() % (int)(bounds.size.width * .8), rand() % (int)(bounds.size.height * .8))];*/
            [bricks addObject:b];
            lasty = ycoor;
            lastx = xcoor;
            //i++;
        }
    int randy = arc4random_uniform(bounds.size.height);
    int randx = arc4random_uniform(bounds.size.width);
    pow = [[Powerup alloc] initWithFrame:CGRectMake(randx, randy, 40, 40)];
    [pow setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"memer.png"]]];
    [self addSubview:pow];
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
/*- (void)drawRect:(CGRect)rect {
    // Drawing code
    [self.subimage drawAtPoint:CGPointMake(x,y)];
}*/

-(void)arrange:(CADisplayLink *)sender
{
    //bounce = 30;
    //Timer *timer;
    CFTimeInterval ts = [sender timestamp];
   // [jumper changePic];
   // [jumper setBackgroundColor:[UIColor blueColor]];
    CGRect bounds = [self bounds];
    
    // Apply gravity to the acceleration of the jumper
    [jumper setDy:[jumper dy] - .3];
    
    // Apply the tilt.  Limit maximum tilt to + or - 5
    [jumper setDx:[jumper dx] + tilt];
    if ([jumper dx] > 5)
        [jumper setDx:5];
    if ([jumper dx] < -5)
        [jumper setDx:-5];
    
    // Jumper moves in the direction of gravity
    CGPoint p = [jumper center];
    p.x += [jumper dx];
    p.y -= [jumper dy];
    
    // If the jumper has fallen below the bottom of the screen,
    // add a positive velocity to move him
    if (p.y > bounds.size.height)
    {
        [jumper setDy:10];
        p.y = bounds.size.height;
    }
    
    // If we've gone past the top of the screen, wrap around
    if (p.y < 0){
        p.y += bounds.size.height;
        [self makeBricks:nil];
        bounce = 15;
    }
    
    CGRect up = [pow frame];
    if(CGRectContainsPoint(up, p)){
        [pow removeFromSuperview];
        [jumper setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"babyr.png"]]];
        [jumper setDy:20];
       // [self performSelectorOnMainThread:@selector(samich:) withObject:NULL waitUntilDone:NO];
    }
    
    
    // If we have gone too far left, or too far right, wrap around
    if (p.x < 0)
        p.x += bounds.size.width;
    if (p.x > bounds.size.width)
        p.x -= bounds.size.width;
    
    
    // If we are moving down, and we touch a brick, we get
    // a jump to push us up.
    if ([jumper dy] < 0 && bounce > 0)
    {
        [jumper setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"kappav2.png"]]];
        for (Brick *brick in bricks)
        {
            CGRect b = [brick frame];
            if (CGRectContainsPoint(b, p))
            {
                // Yay!  Bounce!
                NSLog(@"Bounce!");
                if(bounce > 0){
                    [jumper setDy:10];
                    bheight += 10;
                    bounce -= 1;
                }
                
            }
        }
    }
    //for when the player loses
    else if([jumper dy] < 0 && bounce <= 0){
        for (Brick *brick in bricks)
        {
            [brick removeFromSuperview];
        }
        [jumper removeFromSuperview];
        [pow removeFromSuperview];
        [self setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"gameo.png"]]];
        mess.text = [NSString stringWithFormat:@"You ran outta hops foo"];
    }
    x = p.x;
    y = p.y;
    bcount.text = [NSString stringWithFormat:@"%d", bounce];
    [jumper setCenter:p];
    
    // NSLog(@"Timestamp %f", ts);
}

//-(IBAction)samich{
    /*NSString *soundPath = [[NSBundle mainBundle] pathForResource:@"samich" ofType:@"aif"];
    NSURL *soundURL = [NSURL fileURLWithPath:soundPath];
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)soundURL, &_sound);
    AudioServicesPlaySystemSound(self.sound);
*/
 //    }

@end
