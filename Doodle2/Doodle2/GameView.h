//
//  GameView.h
//  Doodle2
//
//  Created by Patrick Madden on 2/4/17.
//  Copyright © 2017 Binghamton CSD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Jumper.h"
#import "Brick.h"
#import "Powerup.h"
#import <audioToolbox/AudioToolbox.h>
//#import "ViewController.h"

@interface GameView : UIView {
    int x;
    int y;

}
@property (nonatomic, strong) Jumper *jumper;
@property (nonatomic, strong) Powerup *pow;
@property (nonatomic, strong) NSMutableArray *bricks;
@property (nonatomic) float tilt;
@property (nonatomic) int bheight;
@property (nonatomic, strong) IBOutlet UIImage *subimage;
@property (nonatomic, strong) IBOutlet UILabel *bcount, *mess;
-(void)arrange:(CADisplayLink *)sender;
//-(IBAction)samich:(id)sender;
@end
